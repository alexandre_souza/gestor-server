const Entidade = require('../models').Entidade;
const Endereco = require('../models').Endereco;
const Contato = require('../models').Contato;

module.exports = {
  create(req, res) {
    console.log('REQ ------------ > ' + req);
    return Entidade
      .create({
        codigofilial: req.body.codigofilial,
        situacao: req.body.situacao,
        apelido: req.body.apelido,
        nome: req.body.nome,
        nomemae: req.body.nomemae,
        nomepai: req.body.nomepai,    
        fisicajuridica: req.body.fisicajuridica,
        datanascimento: req.body.datanascimento,
        sexo: req.body.sexo,
        estadocivil: req.body.estadocivil,
        nacionalidade: req.body.nacionalidade,
        naturalidade: req.body.naturalidade,
        tratadocomo: req.body.tratadocomo,
        profissao: req.body.profissao,
        cpfcnpj: req.body.cpfcnpj,
        ie: req.body.ie,
        iedatainscricao: req.body.iedatainscricao,
        im: req.body.im,
        imdatainscricao: req.body.imdatainscricao,
        rg: req.body.rg,
        rgtipo: req.body.rgtipo,
        rgdataemissao: req.body.rgdataemissao,
        cnh: req.body.cnh,
        cnhvalidade: req.body.cnhvalidade,
        inscricaoestadual: req.body.inscricaoestadual,
        observacao: req.body.observacao
      })
      .then(Entidade => res.status(201).send(Entidade))
      .catch(error => res.status(400).send(error));
  },
  list(req, res) {
    return Entidade
      .findAll({
            include: [{
              model: Endereco,
              as: 'endereco',
            }],
          })
      .then(Entidades => res.status(200).send(Entidades))
      .catch(error => res.status(400).send(error));
  },
  retrieve(req, res) {
    return Entidade
      .findById(req.params.entidadeId, {
        include: [{
          model: Endereco,
          as: 'endereco',
        }],
      })
      .then(Entidade => {
        if (!Entidade) {
          return res.status(404).send({
            message: 'Entidade Not Found',
          });
        }
        return res.status(200).send(Entidade);
      })
      .catch(error => res.status(400).send(error));
  },
  update(req, res) {
    return Entidade
      .findById(req.params.entidadeId, {
        include: [{
          model: Endereco,
          as: 'endereco',
        }],
      })
      .then(Entidade => {
        if (!Entidade) {
          return res.status(404).send({
            message: 'Entidade Not Found',
          });
        }
        return Entidade
          .update({
            codigofilial: req.body.codigofilial || Entidade.codigofilial,
            situacao: req.body.situacao || Entidade.situacao,
            apelido: req.body.apelido || Entidade.apelido,
            nome: req.body.nome || Entidade.nome,
            nomemae: req.body.nomemae || Entidade.nomemae,
            nomepai: req.body.nomepai || Entidade.nomepai,
            fisicajuridica: req.body.fisicajuridica || Entidade.fisicajuridica,
            datanascimento: req.body.datanascimento || Entidade.datanascimento,
            sexo: req.body.sexo || Entidade.sexo,
            estadocivil: req.body.estadocivil || Entidade.estadocivil,
            nacionalidade: req.body.nacionalidade || Entidade.nacionalidade,
            naturalidade: req.body.naturalidade || Entidade.naturalidade,
            tratadocomo: req.body.tratadocomo || Entidade.tratadocomo,
            profissao: req.body.profissao || Entidade.profissao,
            cpfcnpj: req.body.cpfcnpj || Entidade.cpfcnpj,
            ie: req.body.ie || Entidade.ie,
            iedatainscricao: req.body.iedatainscricao || Entidade.iedatainscricao,
            im: req.body.im || Entidade.im,
            imdatainscricao: req.body.imdatainscricao || Entidade.imdatainscricao,
            rg: req.body.rg || Entidade.rg,
            rgtipo: req.body.rgtipo || Entidade.rgtipo,
            rgdataemissao: req.body.rgdataemissao || Entidade.rgdataemissao,
            cnh: req.body.cnh || Entidade.cnh,
            cnhvalidade: req.body.cnhvalidade || Entidade.cnhvalidade,
            inscricaoestadual: req.body.inscricaoestadual || Entidade.inscricaoestadual,
            observacao: req.body.observacao || Entidade.observacao            
          })
          .then(() => res.status(200).send(Entidade))  // Send back the updated Entidade.
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
  destroy(req, res) {
    return Entidade
      .findById(req.params.entidadeId)
      .then(Entidade => {
        if (!Entidade) {
          return res.status(400).send({
            message: 'Entidade Not Found',
          });
        }
        return Entidade
          .destroy()
          .then(() => res.status(204).send())
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  }
};