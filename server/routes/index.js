// const todosController = require('../controllers').todos;
// const todoItemsController = require('../controllers').todoItems;
const entidadesController = require('../controllers').entidades;

module.exports = (app) => {
  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the Todos API!',
  }));

  app.post('/api/entidade', entidadesController.create);
  app.get('/api/entidade', entidadesController.list);
  app.get('/api/entidade/:entidadeId', entidadesController.retrieve);
  app.put('/api/entidade/:entidadeId', entidadesController.update);
  app.delete('/api/entidade/:entidadeId', entidadesController.destroy);

  // app.post('/api/todos', todosController.create);
  // app.get('/api/todos', todosController.list);
  // app.get('/api/todos/:todoId', todosController.retrieve);
  // app.put('/api/todos/:todoId', todosController.update);
  // app.delete('/api/todos/:todoId', todosController.destroy);

  // app.post('/api/todos/:todoId/items', todoItemsController.create);
  // app.put('/api/todos/:todoId/items/:todoItemId', todoItemsController.update);
  // app.delete(
  //   '/api/todos/:todoId/items/:todoItemId', todoItemsController.destroy
  // );
  
  // For any other request method on todo items, we're going to return "Method Not Allowed"
  // app.all('/api/todos/:todoId/items', (req, res) =>
  //   res.status(405).send({
  //     message: 'Method Not Allowed',
  // }));
};