'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Entidades', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      codigofilial: {
        type: Sequelize.INTEGER
      },
      situacao: {
        type: Sequelize.INTEGER
      },
      apelido: {
        type: Sequelize.STRING
      },
      nome: {
        type: Sequelize.STRING
      },
      nomemae: {
        type: Sequelize.STRING
      },
      nomepai: {
        type: Sequelize.STRING
      },
      fisicajuridica: {
        type: Sequelize.STRING
      },
      datanascimento: {
        type: Sequelize.DATE
      },
      sexo: {
        type: Sequelize.STRING
      },
      estadocivil: {
        type: Sequelize.STRING
      },
      nacionalidade: {
        type: Sequelize.STRING
      },
      naturalidade: {
        type: Sequelize.STRING
      },
      tratadocomo: {
        type: Sequelize.STRING
      },
      profissao: {
        type: Sequelize.STRING
      },
      cpfcnpj: {
        type: Sequelize.STRING
      },
      ie: {
        type: Sequelize.STRING
      },
      iedatainscricao: {
        type: Sequelize.DATE
      },
      im: {
        type: Sequelize.STRING
      },
      imdatainscricao: {
        type: Sequelize.DATE
      },
      rg: {
        type: Sequelize.STRING
      },
      rgtipo: {
        type: Sequelize.STRING
      },
      rgdataemissao: {
        type: Sequelize.DATE
      },
      cnh: {
        type: Sequelize.STRING
      },
      cnhvalidade: {
        type: Sequelize.DATE
      },
      inscricaoestadual: {
        type: Sequelize.STRING
      },
      observacao: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Entidades');
  }
};