'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Enderecos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      situacao: {
        type: Sequelize.STRING
      },
      tipoendereco: {
        type: Sequelize.INTEGER
      },
      tipologradouro: {
        type: Sequelize.STRING
      },
      logradouro: {
        type: Sequelize.STRING
      },
      numero: {
        type: Sequelize.STRING
      },
      complemento: {
        type: Sequelize.STRING
      },
      bairro: {
        type: Sequelize.STRING
      },
      cep: {
        type: Sequelize.STRING
      },
      cepestrangeiro: {
        type: Sequelize.STRING
      },
      uf: {
        type: Sequelize.STRING
      },
      ufibge: {
        type: Sequelize.STRING
      },
      cidade: {
        type: Sequelize.STRING
      },
      cidadeibge: {
        type: Sequelize.STRING
      },
      pais: {
        type: Sequelize.STRING
      },
      paisibge: {
        type: Sequelize.STRING
      },
      entidadeId: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'Entidades',
          key: 'id',
          as: 'entidadeId',
        },
      },       
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Enderecos');
  }
};