'use strict';
module.exports = (sequelize, DataTypes) => {
  var Endereco = sequelize.define('Endereco', {
    situacao: DataTypes.STRING,
    tipoendereco: DataTypes.INTEGER,
    tipologradouro: DataTypes.STRING,
    logradouro: DataTypes.STRING,
    numero: DataTypes.STRING,
    complemento: DataTypes.STRING,
    bairro: DataTypes.STRING,
    cep: DataTypes.STRING,
    cepestrangeiro: DataTypes.STRING,
    uf: DataTypes.STRING,
    ufibge: DataTypes.STRING,
    cidade: DataTypes.STRING,
    cidadeibge: DataTypes.STRING,
    pais: DataTypes.STRING,
    paisibge: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  Endereco.associate = (models) => {
    Endereco.belongsTo(models.Entidade, {
      foreignKey: 'entidadeId',
      onDelete: 'CASCADE',
    });
  };
  return Endereco;
};