'use strict';
module.exports = (sequelize, DataTypes) => {
  var Contato = sequelize.define('Contato', {
    situacao: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  Contato.associate = (models) => {
    Contato.belongsTo(models.Entidade, {
      foreignKey: 'entidadeId',
      onDelete: 'CASCADE',
    });
  };  
  return Contato;
};