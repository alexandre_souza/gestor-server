'use strict';
module.exports = (sequelize, DataTypes) => {
  var Entidade = sequelize.define('Entidade', {
    codigofilial: DataTypes.INTEGER,
    situacao: DataTypes.INTEGER,
    apelido: DataTypes.STRING,
    nome: DataTypes.STRING,
    nomemae: DataTypes.STRING,
    nomepai: DataTypes.STRING,
    fisicajuridica: DataTypes.STRING,
    datanascimento: DataTypes.DATE,
    sexo: DataTypes.STRING,
    estadocivil: DataTypes.STRING,
    nacionalidade: DataTypes.STRING,
    naturalidade: DataTypes.STRING,
    tratadocomo: DataTypes.STRING,
    profissao: DataTypes.STRING,
    cpfcnpj: DataTypes.STRING,
    ie: DataTypes.STRING,
    iedatainscricao: DataTypes.DATE,
    im: DataTypes.STRING,
    imdatainscricao: DataTypes.DATE,
    rg: DataTypes.STRING,
    rgtipo: DataTypes.STRING,
    rgdataemissao: DataTypes.DATE,
    cnh: DataTypes.STRING,
    cnhvalidade: DataTypes.DATE,
    inscricaoestadual: DataTypes.STRING,
    observacao: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  Entidade.associate = (models) => {
    Entidade.hasMany(models.Contato, {
      foreignKey: 'entidadeId',
      as: 'contato',
    });
  };   
  Entidade.associate = (models) => {
    Entidade.hasMany(models.Endereco, {
      foreignKey: 'entidadeId',
      as: 'endereco',
    });
  };  
  return Entidade;
};